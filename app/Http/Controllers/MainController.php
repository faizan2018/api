<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;


class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('cors');
    }

    public function dashboard(){
    	$user = \Auth::user();
    	if($user['admin_role']==0){
    		$role = "Normal user";
    		$privilege = "false";
    	}
    	else{
    		$role = "Admin";
    		$privilege = "true";
    	}
    	if($user['approved']==0)
    		$approval = "not-approved";
    	else
    		$approval = "approved";
    	return response()->json([
    		"name" => $user['name'],
    		"role" => $role,
    		"privilege" => $privilege,
    		"approval" => $approval
    	]);
    }

    public function userlist(){
    	$user = \Auth::user();
    	try{
    		if($user['admin_role']==1){
    			$users = User::paginate(10);
    			return response()->json([$users]);
    		}
    		else{
    			return response()->json(["message" => "You are not authorised to do so"]);
    		}
    	}
    	catch(\Exception $e){
    		abort(404);
    	}
    }

    public function addUser(Request $request){
    	$user = \Auth::user();
    	try{
    		if($user['admin_role']==1 && $user['approved']==1){
    			if(User::create([
            		'name' => $request['name'],
            		'email' => $request['email'],
            		'password' => Hash::make($request->password),
            		'admin_role' => $request->role
        		]))
    			return response()->json(["message" => "User added successfully!"]);
    			return response()->json(["message" => "Something went wrong!"]);
    		}
    		else{
    			return response()->json(["message" => "You are not authorised to do so"]);
    		}
    	}
    	catch(\Exception $e){
    		abort(404);
    	}
    }



    public function updateUser($id, Request $request){
    	$user = \Auth::user();
    	try{
    		if($user['admin_role']==1 && $user['approved']==1){
    			$my_user = User::find($id);
    			$my_user->name = $request['name'];
    			$my_user->email = $request['email'];
    			$my_user->password = Hash::make($request->password);
    			if($my_user->update())
    			return response()->json(["message" => "User details updated successfully!"]);
    			return response()->json(["message" => "Something went wrong!"]);
    		}
    		else{
    			return response()->json(["message" => "You are not authorised to do so"]);
    		}
    	}
    	catch(\Exception $e){
    		abort(404);
    	}
    }

    public function deleteUser($id){
    	$user = \Auth::user();
    	try{
    		if($user['admin_role']==1 && $user['approved']==1){
    			$my_user = User::find($id);
    			if($my_user->delete())
    			return response()->json(["message" => "User details deleted successfully!"]);
    			return response()->json(["message" => "Something went wrong!"]);
    		}
    		else{
    			return response()->json(["message" => "You are not authorised to do so"]);
    		}
    	}
    	catch(\Exception $e){
    		abort(404);
    	}
    }


    public function approveUser($id){
    	$user = \Auth::user();
    	try{
    		if($user['admin_role']==1 && $user['approved']==1){
    			$my_user = User::find($id);
    			$my_user->approved = true;
    			if($my_user->update())
    			return response()->json(["message" => "User approved successfully!"]);
    			return response()->json(["message" => "Something went wrong!"]);
    		}
    		else{
    			return response()->json(["message" => "You are not authorised to do so"]);
    		}
    	}
    	catch(\Exception $e){
    		abort(404);
    	}
    }

    public function disapproveUser($id){
    	$user = \Auth::user();
    	try{
    		if($user['admin_role']==1 && $user['approved']==1){
    			$my_user = User::find($id);
    			$my_user->approved = false;
    			if($my_user->update())
    			return response()->json(["message" => "User disapproved successfully!"]);
    			return response()->json(["message" => "Something went wrong!"]);
    		}
    		else{
    			return response()->json(["message" => "You are not authorised to do so"]);
    		}
    	}
    	catch(\Exception $e){
    		abort(404);
    	}
    }

    public function switchRole($id){
        $user = \Auth::user();
        try{
            if($user['admin_role']==1 && $user['approved']==1){
                $my_user = User::find($id);
                if($my_user->admin_role == false)
                    $my_user->admin_role = true;
                else
                    $my_user->admin_role = false;
                if($my_user->update())
                return response()->json(["message" => "Role switched successfully!"]);
                return response()->json(["message" => "Something went wrong!"]);
            }
            else{
                return response()->json(["message" => "You are not authorised to do so"]);
            }
        }
        catch(\Exception $e){
            abort(404);
        }
    }

}    