<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;


class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
      $this->middleware('cors');
    }

     public function register(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);
        try{
            if(User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request->password)
        ]))
            return response()->json(["output" => "Success"]);
        else
            return response()->json(["output" => "Something went wrong"]); 
        }
        catch(\Exception $e){
            return response()->json(["output" => "User already exists or something went wrong"]);
        }
              
    }

    public function login(Request $request){
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required'
        ]);

       $user = User::where('email', $request->email)->first();
     if(Hash::check($request->password,$user['password'])){
          $apikey = base64_encode(str_random(40));
 
          User::where('email', $request->email)->update(['api_token' => $apikey]);
 
          return response()->json(['status' => 'success','api_token' => $apikey]);
 
      }else{
 
          return response()->json(['status' => 'fail'],401);
 
      }
    }

    //
}
