<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/register',['middleware' => 'cors', 'uses'=>'UsersController@register']);

$router->post('/login', ['middleware' => 'cors', 'uses' => 'UsersController@login']);

$router->get('/dashboard',['middleware' => 'auth', 'uses' => 'MainController@dashboard']);

$router->get('/userList',['middleware' => 'auth', 'uses' => 'MainController@userlist']);

$router->post('/addUser',['middleware' => 'auth', 'uses' => 'MainController@addUser']);

$router->post('/updateUser/{id}',['middleware' => 'auth', 'uses' => 'MainController@updateUser']);

$router->get('/deleteUser/{id}',['middleware' => 'auth', 'uses' => 'MainController@deleteUser']);

$router->get('/approveUser/{id}',['middleware' => 'auth', 'uses' => 'MainController@approveUser']);

$router->get('/disapproveUser/{id}',['middleware' => 'auth', 'uses' => 'MainController@disapproveUser']);

$router->get('/switchRole/{id}',['middleware' => 'auth', 'uses' => 'MainController@switchRole']);
